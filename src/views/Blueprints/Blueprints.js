import React from "react";
import PropTypes from "prop-types";
// @material-ui/core
import withStyles from "@material-ui/core/styles/withStyles";
import Grid from "@material-ui/core/Grid";

import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'

// core components
import GridItem from "components/Grid/GridItem.jsx";

import styles from './styles'
import ItemCard from "components/Item/ItemCard";

const enhance = compose(
  withStyles(styles),
  firestoreConnect([
    { collection: 'blueprints' }
  ]),
  connect(
    ({ firestore }) => ({
      blueprints: firestore.ordered.blueprints,
    })
  )
)

const Blueprints = ({ classes, blueprints }) => (
  <div>
    <Grid container>
      {
        !isLoaded(blueprints)
          ? 'Loading'
          : isEmpty(blueprints)
            ? 'blueprints list is empty'
            : blueprints.map((blueprint) => (
              <GridItem xs={12} sm={6} md={4} key={blueprint.id}>
                <ItemCard item={blueprint} />
              </GridItem>
            ))
      }
    </Grid>
  </div>
)

Blueprints.propTypes = {
  firestore: PropTypes.shape({}),
  blueprints: PropTypes.array,
  classes: PropTypes.object.isRequired
}

export default enhance(Blueprints);
