import types from './types'

const initialState = {
  blueprints: [],
  resources: []
}

function reducer(state = initialState, { type, blueprint, resourceValue }) {
  let blueprints = state.blueprints.map(bp => ({ ...bp }))
  let resources = state.resources.map(r => ({ ...r }))

  let blueprintIndex = blueprints.findIndex(bp => bp.id === blueprint.id)

  switch (type) {
    case types.ADD_BLUEPRINT:
      if (blueprintIndex !== -1) blueprints[blueprintIndex].amount += 1
      else blueprints = [...blueprints, { ...blueprint, amount: 1 }]
      Object.keys(resourceValue).forEach(resource => {
        let { name, amount } = resourceValue[resource]
        let resourceIndex = resources.findIndex(r => r.name === name)
        if (resourceIndex !== -1) resources[resourceIndex].amount += amount
        else resources = [...resources, { name, amount }]
      })
      return Object.assign({}, state, {
        blueprints,
        resources
      })
    case types.REMOVE_BLUEPRINT:
      if (blueprintIndex === -1) return state
      else if (blueprints[blueprintIndex].amount === 1) blueprints.splice(blueprintIndex, 1)
      else blueprints[blueprintIndex].amount -= 1
      Object.keys(resourceValue).forEach(resource => {
        let { name, amount } = resourceValue[resource]
        let resourceIndex = resources.findIndex(r => r.name === name)
        if (resourceIndex !== -1) resources[resourceIndex].amount -= amount
        if (resources[resourceIndex].amount <= 0) resources.splice(resourceIndex, 1)
      })
      return Object.assign({}, state, {
        blueprints,
        resources
      })
    case types.CLEAR_BLUEPRINT:
      if (blueprintIndex === -1) return state
      else {
        Object.keys(resourceValue).forEach(resource => {
          let { name, amount } = resourceValue[resource]
          let resourceIndex = resources.findIndex(r => r.name === name)
          if (resourceIndex !== -1) resources[resourceIndex].amount -= (amount * blueprints[blueprintIndex].amount)
          if (resources[resourceIndex].amount <= 0) resources.splice(resourceIndex, 1)
        })
        blueprints.splice(blueprintIndex, 1)
      }

      return Object.assign({}, state, {
        blueprints,
        resources
      })
    default:
      return state
  }
}

export default reducer