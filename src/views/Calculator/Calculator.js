import React from "react";
import PropTypes from "prop-types";
// @material-ui/core
import withStyles from "@material-ui/core/styles/withStyles";

import { compose } from 'redux'
import { withReducer } from 'recompose'
import { connect } from 'react-redux'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'

// core components
import GridItem from "components/Grid/GridItem.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import Table from "components/Table/Table.jsx";
import Grid from "@material-ui/core/Grid";
import Button from "components/CustomButtons/Button"

import {
  ExposureNeg1,
  ExposureZero,
  ExposurePlus1
} from "@material-ui/icons"

import reducer from './reducer'
import types from './types'

import styles from './styles'
import ItemCard from "components/Item/ItemCard";
import { Tooltip } from "@material-ui/core";

const enhance = compose(
  withStyles(styles),
  firestoreConnect([
    { collection: 'blueprints' },
    { collection: 'refined resources' }
  ]),
  connect(
    ({ firestore }) => ({
      blueprints: firestore.ordered.blueprints,
      refined: firestore.ordered.refined,
      resources: firestore.ordered.resources
    })
  ),
  withReducer('state', 'dispatch', reducer)
)

const calculateResources = (dataStores, { dispatch, type, blueprint }) => {
  let resourceValue = getResourceValue(blueprint)

  dispatch({ type, blueprint, resourceValue })

  function getResourceValue(item) {
    let itemResourceValue = []
    item.requires.forEach(requirement => {
      if (requirement.type === 'resource') {
        let resourceIndex = itemResourceValue.findIndex(rv => rv.name === requirement.name)
        resourceIndex !== -1
          ? itemResourceValue[resourceIndex].amount += requirement.amount
          : itemResourceValue = [...itemResourceValue, requirement]
      } else {
        let requirementResourceValue = getResourceValue(
          dataStores[requirement.type.replace(' ', '_') + 's'].find(bp => bp.id === requirement.name))

        Object.keys(requirementResourceValue).forEach(resource => {
          let { name, amount } = requirementResourceValue[resource]
          let resourceIndex = itemResourceValue.findIndex(rv => rv.name === name)
          amount *= requirement.amount
          resourceIndex !== -1
            ? itemResourceValue[resourceIndex].amount += amount
            : itemResourceValue = [...itemResourceValue, { name, amount }]
        })
      }
    })
    return itemResourceValue
  }
}

const footerButtons = ({ blueprints, refined_resources }, dispatch, blueprint, classes) => (
  <div className={classes.stats}>
    <Tooltip title="Remove from list" placement="bottom">
      <Button justIcon round color="info" onClick={
        () => calculateResources({ blueprints, refined_resources }, { dispatch, type: types.REMOVE_BLUEPRINT, blueprint })
      }>
        <ExposureNeg1 className={classes.icons} />
      </Button>
    </Tooltip>

    <Tooltip title="Clear blueprint" placement="bottom">
      <Button justIcon round color="info" onClick={
        () => calculateResources({ blueprints, refined_resources }, { dispatch, type: types.CLEAR_BLUEPRINT, blueprint })
      }>
        <ExposureZero className={classes.icons} />
      </Button>
    </Tooltip>

    <Tooltip title="Add to list" placement="bottom">
      <Button justIcon round color="info" onClick={
        () => calculateResources({ blueprints, refined_resources }, { dispatch, type: types.ADD_BLUEPRINT, blueprint })
      }>
        <ExposurePlus1 className={classes.icons} />
      </Button>
    </Tooltip>
  </div>
)

const Calculator = ({ classes, blueprints, refined_resources, state, dispatch }) => (
  <Grid container>
    <GridItem xs={12} sm={6} md={6} style={{ padding: 0 }}>
      <Grid container>
        {
          !isLoaded(blueprints)
            ? 'Loading'
            : isEmpty(blueprints)
              ? 'blueprints list is empty'
              : blueprints.map((blueprint) => (
                <GridItem xs={12} sm={6} md={6} key={blueprint.id}>
                  <ItemCard item={blueprint} footer={footerButtons({ blueprints, refined_resources }, dispatch, blueprint, classes)} />
                </GridItem>
              ))
        }
      </Grid>
    </GridItem>
    <GridItem xs={12} sm={6} md={6}>
      <Card className={classes.stickyPanel}>
        <CardHeader color="info">
          <h4 className={classes.cardTitleWhite}>
            Resources Needed
            </h4>
        </CardHeader>
        <CardBody>
          <Grid container>
            <GridItem xs={12} sm={6} md={6}>
              <Table
                tableHeaderColor="info"
                tableHead={["Blueprint", "Amount"]}
                tableData={
                  state.blueprints.map(bp => [bp.id, '' + bp.amount])
                }
              />
            </GridItem>
            <GridItem xs={12} sm={6} md={6}>
              <Table
                tableHeaderColor="info"
                tableHead={["Resource", "Quantity"]}
                tableData={
                  state.resources.map(r => [r.name, '' + r.amount])
                }
              />
            </GridItem>
          </Grid>
        </CardBody>
      </Card>
    </GridItem>
  </Grid>
)

Calculator.propTypes = {
  firestore: PropTypes.shape({}),
  blueprints: PropTypes.array,
  refined_resources: PropTypes.array,
  classes: PropTypes.object.isRequired
}

export default enhance(Calculator);
