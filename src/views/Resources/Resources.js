import React from "react";
import PropTypes from "prop-types";
// @material-ui/core
import withStyles from "@material-ui/core/styles/withStyles";
import Grid from "@material-ui/core/Grid";

import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'

// core components
import GridItem from "components/Grid/GridItem.jsx";

import styles from './styles'
import ItemCard from "components/Item/ItemCard";

const enhance = compose(
  withStyles(styles),
  firestoreConnect([
    { collection: 'resources' }
  ]),
  connect(
    ({ firestore }) => ({
      resources: firestore.ordered.resources,
    })
  )
)

const Resources = ({ classes, resources }) => (
  <div>
    <Grid container>
      {
        !isLoaded(resources)
          ? 'Loading'
          : isEmpty(resources)
            ? 'resources list is empty'
            : resources.map((resource) => (
              <GridItem xs={12} sm={6} md={4} key={resource.id}>
                <ItemCard item={resource} />
              </GridItem>
            ))
      }
    </Grid>
  </div>
)

Resources.propTypes = {
  firestore: PropTypes.shape({}),
  resources: PropTypes.array,
  classes: PropTypes.object.isRequired
}

export default enhance(Resources);
