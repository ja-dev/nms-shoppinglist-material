import React from "react";
import PropTypes from "prop-types";
// @material-ui/core
import withStyles from "@material-ui/core/styles/withStyles";
import Grid from "@material-ui/core/Grid";

import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'

// core components
import GridItem from "components/Grid/GridItem.jsx";

import styles from './styles'
import ItemCard from "components/Item/ItemCard";

const enhance = compose(
  withStyles(styles),
  firestoreConnect([
    { collection: 'refined resources' }
  ]),
  connect(
    ({ firestore }) => ({
      refined_resources: firestore.ordered['refined resources'],
    })
  )
)

const RefinedResources = ({ classes, refined_resources }) => (
  <div>
    <Grid container>
      {
        !isLoaded(refined_resources)
          ? 'Loading'
          : isEmpty(refined_resources)
            ? 'refined list is empty'
            : refined_resources.map((resource) => (
              <GridItem xs={12} sm={6} md={4} key={resource.id}>
                <ItemCard item={resource} />
              </GridItem>
            ))
      }
    </Grid>
  </div>
)

RefinedResources.propTypes = {
  firestore: PropTypes.shape({}),
  refined_resources: PropTypes.array,
  classes: PropTypes.object.isRequired
}

export default enhance(RefinedResources);
