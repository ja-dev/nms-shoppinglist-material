export const firebase = {
  apiKey: "AIzaSyA1qoEgKzaRTAntJa32kVLJVNPke3Yq7yM",
  authDomain: "no-mans-sky-shopping-list.firebaseapp.com",
  databaseURL: "https://no-mans-sky-shopping-list.firebaseio.com",
  projectId: "no-mans-sky-shopping-list",
  storageBucket: "no-mans-sky-shopping-list.appspot.com",
  messagingSenderId: "229420008530"
}

export const rrfConfig = {
  userProfile: 'users',
  useFirestoreForProfile: true, // Store in Firestore instead of Real Time DB
  enableLogging: false
}

export default { firebase, rrfConfig }