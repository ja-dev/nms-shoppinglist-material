import React from "react";
import PropTypes from "prop-types";
// @material-ui/core
import withStyles from "@material-ui/core/styles/withStyles";

import { compose } from 'redux'

// core components
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardIcon from "components/Card/CardIcon.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";

import styles from './styles'
import unitsSymbol from 'assets/img/units_symbol.png'

const enhance = compose(
  withStyles(styles)
)

const ItemCard = ({ classes, item, footer }) => (
  <Card>
    <CardHeader stats icon>
      <CardIcon color={item.category || 'info'}>
        <img src={item.imageUrl} className={classes.cardImage} alt={item.id} />
      </CardIcon>
      <p className={classes.cardCategory}>{item.type}</p>
      <h3 className={classes.cardTitle}>
        {item.id}
      </h3>
    </CardHeader>
    {
      item.value && (
        <CardBody className={classes.valueBody}>
          Value: <img src={unitsSymbol} className={classes.unitsSymbolImage} alt="units symbol" />{item.value}
        </CardBody>
      )
    }
    <CardFooter stats>
      {footer}
      {!footer && (
        <div className={classes.stats}>
          {item.requires ? 'Requires:' : 'Sources:'}
          <ul>
            {
              item.requires && item.requires.map(child => (
                <li key={child.name}>
                  {child.amount}x {child.name}
                </li>
              ))
            }
            {
              item.sources && item.sources.map(source => (
                <li key={source}>
                  {source}
                </li>
              ))
            }
          </ul>
        </div>
      )}
    </CardFooter>
  </Card>
)

ItemCard.propTypes = {
  item: PropTypes.object,
  classes: PropTypes.object.isRequired
}

export default enhance(ItemCard);
