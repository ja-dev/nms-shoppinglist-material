import React from 'react'
import PropTypes from 'prop-types'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withHandlers } from 'recompose'
import {
  firestoreConnect,
  isLoaded,
  isEmpty
} from 'react-redux-firebase'

const enhance = compose(
  firestoreConnect([
    // Load events from Firestore
    { collection: 'events' }
  ]),
  connect(
    ({ firestore }) => ({
      events: firestore.ordered.events,
    })
  )
)

const EventSummary = ({ firestore, events }) => (
  <div>
    <h4>events List</h4>
    {
      !isLoaded(events)
        ? 'Loading'
        : isEmpty(events)
          ? 'events list is empty'
          : events.map((event) => { console.log(firestore.get(event.marshals[0].path)); return (event.id) }
          )
    }
  </div>
)

EventSummary.propTypes = {
  firestore: PropTypes.shape({ // from enhnace (withFirestore)
    add: PropTypes.func.isRequired,
  }),
  events: PropTypes.array
}

export default enhance(EventSummary)