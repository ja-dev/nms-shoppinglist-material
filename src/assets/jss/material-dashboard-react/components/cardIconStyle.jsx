import {
  warningCardHeader,
  successCardHeader,
  dangerCardHeader,
  infoCardHeader,
  primaryCardHeader,
  roseCardHeader,

  oxideCardHeader,
  neutralCardHeader,
  componentCardHeader,
  isotopeCardHeader,
  silicateCardHeader
} from "assets/jss/material-dashboard-react.jsx";
const cardIconStyle = {
  cardIcon: {
    "&$silicateCardHeader,&$isotopeCardHeader,&$componentCardHeader,&$neutralCardHeader,&$oxideCardHeader,&$warningCardHeader,&$successCardHeader,&$dangerCardHeader,&$infoCardHeader,&$primaryCardHeader,&$roseCardHeader": {
      borderRadius: "3px",
      backgroundColor: "#999",
      padding: "15px",
      marginTop: "-20px",
      marginRight: "15px",
      float: "left",
      maxHeight: 56
    }
  },
  warningCardHeader,
  successCardHeader,
  dangerCardHeader,
  infoCardHeader,
  primaryCardHeader,
  roseCardHeader,

  oxideCardHeader,
  neutralCardHeader,
  componentCardHeader,
  isotopeCardHeader,
  silicateCardHeader
};

export default cardIconStyle;
