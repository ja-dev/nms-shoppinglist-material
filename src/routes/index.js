import Dashboard from "layouts/Dashboard/Dashboard.js";

const indexRoutes = [{ path: "/", component: Dashboard }];

export default indexRoutes;
