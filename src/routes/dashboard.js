// @material-ui/icons
import {
  Poll,
  Build,
  Cached,
  Whatshot
} from "@material-ui/icons";

// core components/views
import CalculatorPage from "views/Calculator/Calculator.js";
import BlueprintsPage from "views/Blueprints/Blueprints.js";
import RefinedResourcesPage from "views/RefinedResources/RefinedResources.js";
import ResourcesPage from "views/Resources/Resources.js";

const dashboardRoutes = [
  {
    path: "/calculator",
    sidebarName: "Calculator",
    navbarName: "Calculator",
    icon: Poll,
    component: CalculatorPage
  }, {
    path: "/blueprints",
    sidebarName: "Blueprints",
    navbarName: "Blueprints",
    icon: Build,
    component: BlueprintsPage
  }, {
    path: "/refined",
    sidebarName: "Refined Resources",
    navbarName: "Refined Resources",
    icon: Cached,
    component: RefinedResourcesPage
  }, {
    path: "/resources",
    sidebarName: "Resources",
    navbarName: "Resources",
    icon: Whatshot,
    component: ResourcesPage
  },
  { redirect: true, path: "/", to: "/calculator", navbarName: "Redirect" }
];

export default dashboardRoutes;
